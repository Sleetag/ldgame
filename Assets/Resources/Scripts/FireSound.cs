﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireSound : MonoBehaviour
{

    public AudioSource High;
    public AudioSource Low;

    private int FireHealth = 100;
    private const int HealthLimit = 20;

    private LinkedList<AudioSource> audios;
    private GameController game;
    // Start is called before the first frame update
    void Start()
    {
        game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        game.GetFire().fireChanged.AddListener(ChangeSound);
    }

    // Update is called once per frame
    void Update()
    {
        ChangeSound();
    }
    private bool IsFireLow()
    {
        return game.GetFire().IsFireLow();    
    }
    private void ChangeSound()
    {
        if (IsFireLow() && !Low.isPlaying)
        {
            High.Stop();
            Low.Play();
        }
        else if (!IsFireLow() && !High.isPlaying)
        {
            High.Play();
            Low.Stop();
        }
    }
    public void StartFire()
    {
        High.Play();
    }
}
