﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientSound : MonoBehaviour
{
    public AudioSource Birds;
    public AudioSource Cicadas;

    private int nextUpdate=1;
    private const int limit = 50;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RandomPlayCicadas();
      
    }

    private void RandomPlayCicadas()
    {
        if (Time.time >= nextUpdate)
        {
            nextUpdate = Mathf.FloorToInt(Time.time) + 1;
            if (Random.Range(0.0f, 100.0f)>limit && !Cicadas.isPlaying) Cicadas.Play(); 
        }
    }
}
