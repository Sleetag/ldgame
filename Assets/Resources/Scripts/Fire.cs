
﻿using UnityEngine.Events;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public int pv { get; private set; }

    public int pvMax;
    public UnityEvent fireChanged;
    private GameController m_GameController;

    [SerializeField]
    private Animator m_FireAnimator;

    private void heal() {
        if(m_GameController.GetPlayer().GetWood()>0)
        {
            int healHp = 1; // TODO get Player.wood when GameController implemented.
            pv = pv + healHp * m_GameController.GetPlayer().GetWood();

            if (pv > pvMax)
            {
                pv = pvMax;
            }
            fireChanged?.Invoke();
            m_GameController.GetPlayer().DecrementeWood();
        }
    }

    private void FireAnimatorHandler()
    {
        m_FireAnimator.SetInteger("Life", this.pv);
    }

    public void takeDamage(int damage) {
        if(damage > pv) {
            pv = 0;
            fireChanged?.Invoke();
        } else {
            pv = pv - damage;
            fireChanged?.Invoke();
        }
    }

    void ConsumeFire() {
        // On baisse les pv du feu de 1
        if(pv > 0) {
            --pv;
            fireChanged?.Invoke();
        }
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.tag == "Player") {
            collider.gameObject.GetComponent<Player>().chopEvent.AddListener(this.heal);
        }
    }

    private void OnTriggerExit2D(Collider2D collider) {
        if (collider.gameObject.tag == "Player") {
            collider.gameObject.GetComponent<Player>().chopEvent.RemoveListener(this.heal);
        }
    }

    // Start is called before the first frame update
    void Start() {
        //pvMax = 100;
        pv = pvMax;
        Debug.Log("PV " + pv);
        this.fireChanged.AddListener(FireAnimatorHandler);
        InvokeRepeating("ConsumeFire", 5f, 1f);
        m_GameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update() {
        
    }

    public bool IsFireLow()
    {
        return pv < 50;
    }
}
