﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CuttedTreeTiles : MonoBehaviour
{
    private static CuttedTreeTiles _instance;
    private Tilemap cuttedTreeTileMap = null;
    public  Sprite cuttedSprite = null;

    // Start is called before the first frame update
    void Start()
    {
        cuttedTreeTileMap = gameObject.GetComponent<Tilemap>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static CuttedTreeTiles instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType(typeof(CuttedTreeTiles)) as CuttedTreeTiles;
            }

            return _instance;
        }
    }

    public void AddCuttedTreeTile(Vector3Int position, Tile cuttedTreeTile)
    {
        if(cuttedTreeTileMap != null)
        {
            cuttedTreeTile.sprite = cuttedSprite;
            cuttedTreeTileMap.SetTile(position, cuttedTreeTile);
        }
        
    }
}
