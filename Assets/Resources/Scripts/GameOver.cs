﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameOver : MonoBehaviour
{

    private GameController game;
    [SerializeField]
    private Text timer;
    void Awake()
    {
        game = GameObject.FindWithTag("GameController").GetComponent(typeof(GameController)) as GameController;
        game.gameOverEvent.AddListener(ShowCanvas);
        this.gameObject.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void ShowCanvas()
    {
        this.gameObject.SetActive(true);
        string minutes = Mathf.Floor(game.timer / 60).ToString("00");
        string seconds = (game.timer % 60).ToString("00");

        timer.text = string.Format("{0}:{1}", minutes, seconds);
    }
    public void Shop(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    public void ToHome(string sceneName)
    {
        game.Destroy();
        SceneManager.LoadScene(sceneName);
    }
}
