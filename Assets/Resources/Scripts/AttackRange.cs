﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRange : MonoBehaviour
{
    public int damage { private get; set; }
    public bool isAttacking { private get; set; }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (isAttacking && collision.gameObject.tag == "Ennemy")
        {
            Debug.Log("OUCH!");
            Debug.Log(collision);
            Debug.Log(collision.GetComponent<Ennemy>());
            collision.GetComponent<Ennemy>().takeDamage(damage);
        }
    }
}
