﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine;

public class  GameController : MonoBehaviour
{
    public UnityEvent startEvent;
    public UnityEvent pauseEvent;
    public UnityEvent exitEvent;
    public UnityEvent fireEvent;
    public UnityEvent gameOverEvent;

    private float initialFire = 100f;

    public bool isGamePaused = true;
    public bool isGameOver = false;

    private GameObject fireGO;
    private Fire fire;
    private GameObject playerGO;
    private Player player;
    private GameObject treeTilesGO;
    private TreeTiles treeTiles;
    public float timer;

    void Awake()

    {
        DontDestroyOnLoad(this.gameObject);
        isGamePaused = true;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // Start is called before the first frame update
    void Start()
    {
        //StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        if(!isGamePaused)
        {
            //fireEvent.Invoke();
            timer += Time.deltaTime;
        }
    }
    void FetchObject()
    {
        
        playerGO = GameObject.FindWithTag("Player");
        if (playerGO != null)
        {
            player = playerGO.GetComponent(typeof(Player)) as Player;
        }else
        {
            Debug.Log("playerGO Not found");
        }
        fireGO = GameObject.FindWithTag("Fire");
        if (fireGO != null)
        {
            fire = fireGO.GetComponent(typeof(Fire)) as Fire;
            fire.fireChanged.AddListener(UpdateFire);
        }
        else
        {
            Debug.Log("fireGO Not found");
        }
        treeTilesGO = GameObject.FindWithTag("TreeTiles");
        if (treeTilesGO != null)
        {
            treeTiles = treeTilesGO.GetComponent(typeof(TreeTiles)) as TreeTiles;
            treeTiles.treeDownEvent.AddListener(player.IncrementeWood);
        }
        else
        {
            Debug.Log("fireGO Not found");
        }

    }
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name);
        Debug.Log(mode);
        if(!isGamePaused)
        {
            FetchObject();
        }
    }
    public void StartGame()
    {
        startEvent.Invoke();
        isGamePaused = false;
       
    }
    public void PauseGame()
    {
        isGamePaused = true;
        pauseEvent.Invoke();
    }
    public void StopGame()
    {
        exitEvent.Invoke();
    }
    public void UpdateFire()
    {
        if(GetFire().pv<1)
        {
            isGamePaused = true;
            isGameOver = true;
            gameOverEvent?.Invoke();
        }
        fireEvent.Invoke();
    }
    public Fire GetFire()
    {
        if (fireGO == null)
        {
            fireGO = GameObject.FindWithTag("Fire");
            
        }
        if(fireGO != null && fire == null)
        {
            fire = fireGO.GetComponent(typeof(Fire)) as Fire;
            if(fire!=null)fire.fireChanged.AddListener(UpdateFire);
        }
        return fire;
    }
    public Player GetPlayer()
    {
        if(playerGO == null)
        {
            playerGO = GameObject.FindWithTag("Player");
        }
        if (playerGO != null && player == null)
        {
            player = playerGO.GetComponent(typeof(Player)) as Player;
        }
        return player;
    }
    public void Destroy()
    {
        
        Destroy(fireGO.gameObject);
        Destroy(playerGO.gameObject);
        Destroy(treeTilesGO.gameObject);
        Destroy(this);
        Destroy(this.gameObject);
    }
}
