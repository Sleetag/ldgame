﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {
    public GameController game;
    public void StartGame(string sceneName) {
        SceneManager.LoadScene(sceneName);
        game.StartGame();
    }

    public void ExitGame() {
        Application.Quit();
    }
}
