﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    private float m_Speed = 1.3f;

    [SerializeField]
    private int m_Life = 10;

    [SerializeField]
    private Animator m_Animator;

    [SerializeField]
    private AttackRange attack;
    [SerializeField]
    private int strength;

    public UnityEvent chopEvent;
    public UnityEvent axeEvent;
    public UnityEvent walkEvent;

    [SerializeField]
    private Text m_WoodText;
    public UnityEvent incrementEvent;

    private GameController m_GameController; 
    private  int m_Wood = 0;

    public int GetWood()
    {
        return m_Wood;
    }
    public void IncrementeWood()
    {
        m_Wood += Random.Range(2, 3);
        m_Animator.SetInteger("Wood", m_Wood);
        incrementEvent?.Invoke();
    }

    public void DecrementeWood()
    {
        m_Animator.SetInteger("Wood", m_Wood);
        m_Wood = 0;
    }

    void Start()
    {
        m_GameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        attack.damage = strength;
    }

    void Update()
    {
        if (m_GameController.isGameOver) return;
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector2 movement = Vector2.zero;
        if (moveHorizontal > 0){
            movement.x = 1f;
        } else if (moveHorizontal < 0){
            movement.x = -1f;
        }
        if (moveVertical > 0){
            movement.y = 1f;
        }
        else if (moveVertical < 0){
            movement.y = -1f;
        }
        
        if (movement != Vector2.zero)
        {
            m_Animator.SetBool("isWalk", true);
            m_Animator.SetFloat("Horizontal", Input.GetAxis("Horizontal"));
            m_Animator.SetFloat("Vertical", Input.GetAxis("Vertical"));
            walkEvent?.Invoke();
        }
        else
        {
            m_Animator.SetBool("isWalk", false);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            m_Animator.SetBool("Attack", true);
            axeEvent?.Invoke();
            attack.isAttacking = true;
        }
        else
        {
            m_Animator.SetBool("Attack", false);
            attack.isAttacking = false;
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            m_Animator.SetInteger("Wood", m_Wood);
            m_Animator.SetBool("Fire", true);
            chopEvent?.Invoke();

        }
        else if(Input.GetKeyUp(KeyCode.F))
        {
            m_Animator.SetInteger("Wood", m_Wood);
            m_Animator.SetBool("Fire", false);
        }
        if ((movement.x > 0f || movement.x < 0f) && (movement.y > 0f || movement.y < 0f))
            movement /= 1.5f;
        this.transform.Translate(movement * m_Speed * Time.deltaTime);


    }

}
