﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Ennemy : MonoBehaviour
{
    
    public int pv;
    public int damage;


    public Transform target;

    public float speed = 200f;
    public float nextWaypointDistance = 3f;

    private Fire fireTargeted;

    public Sprite[] sprites;

    Path path;
    int currentWaypoint;
    bool reachedEndOfPath = false;


    SpriteRenderer spriteRenderer;
    Seeker seeker;
    Rigidbody2D rigidBody;
    Animator animator;

    public AudioSource walk;
    public AudioSource death;


    // Start is called before the first frame update
    void Start()
    {
        //randomize sprite
        this.gameObject.GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length-1)];

        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Fire").transform;
        }
        seeker = GetComponent<Seeker>();
        rigidBody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();

        InvokeRepeating("NextAction", 0f, 1f);
        
    }

    void OnCollisionEnter2D(Collision2D collision) //When ennemy enter in collision with fire set the fire targeted to the fire in collision
    {
        if (collision.gameObject.tag.ToString() == "Fire")
        {
            fireTargeted = collision.gameObject.GetComponent<Fire>();
            animator.SetBool("isAttacking", true);
        }
    }

    void OnCollisionExit2D(Collision2D collision) //When ennemy quit collision with fire set the fire targeted to null
    {
        if (collision.gameObject.tag.ToString() == "Fire")
        {
            fireTargeted = null;
            animator.SetBool("isAttacking", false);
        }
    }

    public void takeDamage(int damage) {
        pv -= damage;
        if (pv <= 0) {
            animator.SetTrigger("death");
            die();
        }
    }

    public void die() {
        if (!death.isPlaying) death.Play();
        GameObject.Destroy(this.gameObject, 1.5f);
    }

    void NextAction() { // If the ennemy is not close to the fire search the fire else attack the fire
        if(fireTargeted == null)
        {
            seeker.StartPath(rigidBody.position, target.position, OnPathComplete);
        }
        else
        {
            fireTargeted.takeDamage(damage);
            if (!walk.isPlaying) walk.Play();
        }

    }

    void OnPathComplete(Path p){ //Appelé une fois le path calculé
        if (!p.error){
            path = p;
            currentWaypoint = 0;
        }
    }

    

    // Update is called once per frame
    void Update()
    {
        
        if (path == null){
            return;
        }
        if (currentWaypoint >= path.vectorPath.Count){
            reachedEndOfPath = true;
            return;
        }
        else
        {
            
            reachedEndOfPath = false;
        }

        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rigidBody.position).normalized;
        Vector2 force = direction * speed * Time.deltaTime;

        if(force.x < 0f && transform.localScale.x > 0)
        {

            Vector3 newScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, 0);
            transform.localScale = newScale;
            //spriteRenderer.flipX = true;
        }
        else
        {
            if(force.x > 0f && transform.localScale.x < 0)
            {
                Vector3 newScale = new Vector3(transform.localScale.x * -1, transform.localScale.y,0);
                transform.localScale = newScale;
            }
            
            //spriteRenderer.flipX = false;
        }

        rigidBody.AddForce(force);

        float distance = Vector2.Distance(rigidBody.position, path.vectorPath[currentWaypoint]);

        if(distance < nextWaypointDistance) {
            currentWaypoint++;
        }
    }
}
