﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuTrigger : MonoBehaviour
{
    [SerializeField]
    private Animator m_StartButton;

    [SerializeField]
    private Animator m_ExitButton;

    [SerializeField]
    private Animator m_ControlsButton;


    [SerializeField]
    private GameObject m_Controls; 


    public void StartButton(bool value)
    {
        m_StartButton.SetBool("Hover", value);
    }



    public void ExitButton(bool value)
    {
        m_ExitButton.SetBool("Hover", value);
    }

    public void ControlButton(bool value)
    {
        m_ControlsButton.SetBool("Hover", value);
    }

    public void ControlsButton()
    {
        if (m_Controls.activeSelf)
        {
            m_Controls.SetActive(false);
        } else 
        {
            m_Controls.SetActive(true);
        }
    }
}
