﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject monster;
    public GameObject[] monsterList;
    private Vector3[] positions = new Vector3[10] {
        new Vector3(17, -10, 0),
        new Vector3(22, 8, 0),
        new Vector3(23, 13, 0),
        new Vector3(-2, 15.5f, 0),
        new Vector3(-19, 15.5f, 0),
        new Vector3(-22, 8, 0),
        new Vector3(-22, -4, 0),
        new Vector3(-10.5f, -11, 0),
        new Vector3(10, -11.5f, 0),
        new Vector3(-23, 3.5f, 0),
    };

    private Vector3 bossPosition = new Vector3(-12.21f, -4.35f);

    // Start is called before the first frame update
    void Start()
    {
        
        InvokeRepeating("SpawnMonster", 5f, Random.Range(5, 8));
    }

    void SpawnMonster()
    {

        //transform.SetPositionAndRotation(positions[Random.Range(0,positions.Length-1)], Quaternion.identity);
        if(Random.Range(0,30) <= 1)
        {
            Instantiate(monster, bossPosition, Quaternion.identity);
        }
        else
        {
            Instantiate(monsterList[Random.Range(0, monsterList.Length - 1)], positions[Random.Range(0, positions.Length - 1)], Quaternion.identity);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
