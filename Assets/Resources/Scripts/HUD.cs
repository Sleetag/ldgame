using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour {
    private float canvasWidth;
    private float canvasHeight;
    private Vector3 directionToFire;
    private Bounds bounds;

    public float offset; // offset sur Z de la Camera.main
    public Fire fire;
    public Text fireLife;
    public Image fireCursor;
    public Text woodNumber;

    public GameController game;

    private bool isVisible(GameObject Object) {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

        if(GeometryUtility.TestPlanesAABB(planes, bounds)) {
            return true;
        } else {
            return false;
        }
    }

    private void UpdateFire() {
        fireLife.text = "Fire = " + fire.pv + "%";
    }

    // Start is called before the first frame update
    void Start() {
        canvasWidth = ((RectTransform)this.GetComponent<RectTransform>()).rect.width - fireCursor.rectTransform.rect.width;
        canvasHeight = ((RectTransform)this.GetComponent<RectTransform>()).rect.height - fireCursor.rectTransform.rect.height;
        game = GameObject.FindWithTag("GameController").GetComponent(typeof(GameController)) as GameController;
        fire = game.GetFire();
        bounds = fire.GetComponent<PolygonCollider2D>().bounds;
    }

    // Update is called once per frame
    void Update() {
        UpdateFire();
        UpdateWood();

        if (isVisible(fire.gameObject)) {
            fireCursor.enabled = false;
        } else {
            fireCursor.enabled = true;
            directionToFire = fire.transform.position - (Camera.main.transform.position + Vector3.forward * offset);
            float w = canvasWidth * directionToFire.normalized.x / 2;
            float h = canvasHeight * directionToFire.normalized.y / 2;

            fireCursor.rectTransform.localPosition = new Vector3(w, h, 0);
        }
    }
    void UpdateWood()
    {
        woodNumber.text = " " + game.GetPlayer().GetWood();
    }
}
