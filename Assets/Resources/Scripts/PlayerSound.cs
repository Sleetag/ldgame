﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSound : MonoBehaviour
{
   
    public AudioSource chop;
    public AudioSource axe;
    public AudioSource walk;

    public Player player;

    void Awake()
    {
        player = gameObject.GetComponent(typeof(Player)) as Player;
    }

    // Start is called before the first frame update
    void Start()
    {
        player.chopEvent.AddListener(TreeChop);
        player.axeEvent.AddListener(AxeSwipe);
        player.walkEvent.AddListener(WalkAround);
        player.incrementEvent.AddListener(TreeChop);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void TreeChop ()
    {
        if (!chop.isPlaying) chop.Play();
    }
    private void AxeSwipe()
    {
        if (!axe.isPlaying) axe.Play();
    }
    private void WalkAround()
    {
        if (!walk.isPlaying) walk.Play();
    }
}
