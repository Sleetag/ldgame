﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TreeTiles : MonoBehaviour
{
    private Tilemap treeTileMap = null;
    private List<Vector3Int> treePositions = new List<Vector3Int>();
    private ContactPoint2D contactPoint;
    public UnityEvent treeDownEvent;


    private void UpdateTreesPositions()
    {   
        treePositions = new List<Vector3Int>();
        foreach(Vector3Int position in treeTileMap.cellBounds.allPositionsWithin)
        {
            Vector3Int localPlace = new Vector3Int(position.x, position.y, position.z);
            if(treeTileMap.HasTile(localPlace))
            {
                treePositions.Add(localPlace);
            }
        }

    }

    private Vector3Int FindNearestTree(Vector3 playerPosition)
    {
        Vector3 minimalPosition = new Vector3();
        float minDist = Mathf.Infinity;
        foreach (Vector3Int t in treePositions)
        {
            float dist = Vector3.Distance(t, playerPosition);
            if (dist < minDist)
            {
                minimalPosition = t;
                minDist = dist;
            }
        }

        return Vector3Int.CeilToInt(minimalPosition);
    }

    private Tile GetCopyOfTile(Tile tile)
    {
        Tile copy = new Tile();
        copy.colliderType = tile.colliderType;
        copy.sprite = tile.sprite;
        copy.transform = tile.transform;
        copy.name = tile.name;
        copy.gameObject = tile.gameObject;
        copy.color = tile.color;
        copy.flags = tile.flags;
        copy.sprite = tile.sprite;

        return copy;
    }

    // Start is called before the first frame update
    void Start()
    {
        treeTileMap = gameObject.GetComponent<Tilemap>();
        if(treeTileMap != null)
        {
            UpdateTreesPositions();
        }
        //game = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            contactPoint = collision.contacts[0];
            collision.gameObject.GetComponent<Player>().axeEvent.AddListener(this.CutTree);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            collision.gameObject.GetComponent<Player>().axeEvent.RemoveListener(this.CutTree);
        }
    }
    private void CutTree()
    {
        UpdateTreesPositions();
        Vector3 positionOfCollision = treeTileMap.WorldToCell(contactPoint.point);
        Vector3Int collidedTreePosition = FindNearestTree(positionOfCollision);
        Tile tile = treeTileMap.GetTile<Tile>(collidedTreePosition);
        Tile newTile = GetCopyOfTile(tile);
        treeTileMap.SetTile(collidedTreePosition, null);
        CuttedTreeTiles cuttedTreeTiles = CuttedTreeTiles.instance;
        cuttedTreeTiles.AddCuttedTreeTile(collidedTreePosition, newTile);
        treeDownEvent?.Invoke();
    }
}
