﻿using UnityEngine;

public class AmbiantMusicManager : MonoBehaviour {
    private AudioSource audioSource;
    public AudioClip[] ambiantMusics;

    // Start is called before the first frame update
    void Start() {
        audioSource = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() {
    }
}
